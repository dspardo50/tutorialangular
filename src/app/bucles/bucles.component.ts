import { Component, OnInit } from '@angular/core';
import { Persona } from '../persona';
@Component({
  selector: 'app-bucles',
  templateUrl: './bucles.component.html',
  styleUrls: ['./bucles.component.css']
})
export class BuclesComponent implements OnInit {
  personas: Persona[] = [
    {nombre: 'Juan', edad:20},
    {nombre: 'Maria', edad:25},
    {nombre: 'Moises', edad:30},
    {nombre: 'Carlos', edad:45},
  ];



  constructor() {}
  ngOnInit(): void {

  }

}
